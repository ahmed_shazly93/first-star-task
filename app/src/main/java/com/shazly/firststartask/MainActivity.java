package com.shazly.firststartask;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.shazly.firststartask.activity.SignUpActivity;
import com.shazly.firststartask.databinding.ActivityMainBinding;
import com.shazly.firststartask.fragment.ChartFragment;
import com.shazly.firststartask.fragment.CurrentLocationFragment;
import com.shazly.firststartask.fragment.MapFragment;
import com.shazly.firststartask.fragment.ProfileFragment;
import com.shazly.firststartask.utilities.BaseActivity;

import java.util.concurrent.TimeUnit;

public class MainActivity extends BaseActivity<ActivityMainBinding>
        implements NavigationView.OnNavigationItemSelectedListener {
    private int currentPosition = 0;
    private Fragment[] fragments = {
            new MapFragment(),
            new ChartFragment(),
            new CurrentLocationFragment(),
            new ProfileFragment()

    };
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    DrawerLayout drawer;
    Toolbar toolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    private void init() {


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FirebaseMessaging.getInstance().subscribeToTopic("firststartask");

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        currentPosition = getIntent().getIntExtra("currentPosition", 0);

        switchFragment(0);
        setUpToggle();

    }

    public void setUpToggle() {
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }
        };
        toggle.setDrawerIndicatorEnabled(false);

        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_white, this.getTheme());

        toggle.setHomeAsUpIndicator(drawable);

        drawer.addDrawerListener(toggle);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {

                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void switchFragment(int index) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        String tag = fragments[index].getClass().getName();

        if (getSupportFragmentManager().findFragmentByTag(tag) == null) {
            transaction.add(R.id.frame_container, fragments[index], tag);
        }
        transaction.hide(fragments[currentPosition]);
        transaction.show(fragments[index]);
        transaction.commit();
        currentPosition = index;
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {
            if (currentPosition != 0) {
                switchFragment(0);
            }
        } else if (id == R.id.nav_sign_up) {
            startActivity(new Intent(MainActivity.this, SignUpActivity.class));
        } else if (id == R.id.nav_chart) {
            if (currentPosition != 1) {
                switchFragment(1);
            }
        } else if (id == R.id.nav_tracking) {
            if (currentPosition != 2) {
                switchFragment(2);
            }
        } else if (id == R.id.nav_profile) {
            if (currentPosition != 3) {
                switchFragment(3);
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
