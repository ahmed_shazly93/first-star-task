package com.shazly.firststartask.fragment;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.shazly.firststartask.R;
import com.shazly.firststartask.api.RetroService;
import com.shazly.firststartask.api.RetrofitClient;
import com.shazly.firststartask.databinding.FragmentChartBinding;
import com.shazly.firststartask.model.ChartModel;
import com.shazly.firststartask.utilities.BaseFragment;
import com.shazly.firststartask.utilities.ResourceUtil;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends BaseFragment<FragmentChartBinding> {
    RetroService retroService;
    CompositeDisposable compositeDisposable;

    public ChartFragment() {
        // Required empty public constructor
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_chart;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        retroService = RetrofitClient.getClient().create(RetroService.class);
        compositeDisposable = new CompositeDisposable();
        getChartData();
    }

    private void drawLineChart(ArrayList<Entry> yData, ArrayList<String> xData) {
        LineDataSet lineDataSet = new LineDataSet(yData, "Date");
        lineDataSet.setFillColor(getResources().getColor(R.color.colorPrimary));
        lineDataSet.setDrawFilled(true);
        lineDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        LineData lineData = new LineData(xData, lineDataSet);
        lineData.setValueTextSize(15f);
        lineData.setValueTextColor(Color.BLUE);
        getViewDataBinding().lineChart.setData(lineData);
        getViewDataBinding().lineChart.invalidate();
    }

    public void getChartData() {
        if (ResourceUtil.isNetworkAvailable(getActivity())) {
            showProgressDialog();
            compositeDisposable.add(
                    retroService.getChartData("hc6yy233few3eqaa",
                            60007
                    ).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<ChartModel>() {
                                @Override
                                public void onSuccess(ChartModel model) {
                                    dismissProgressDialog();

                                    if (model.getInfo().size() > 0) {
                                        ArrayList<Entry> yData = new ArrayList<>();
                                        ArrayList<String> xData = new ArrayList<>();
                                        for (int i = 0; i < model.getInfo().size(); i++) {
                                            yData.add(new Entry(model.getInfo().get(i).getNumberofOrders(), i));
                                            xData.add(model.getInfo().get(i).getDate());
                                            drawPieChart(yData, xData);
                                            drawLineChart(yData, xData);
                                        }
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialog();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(getActivity()).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }
    }

    private void drawPieChart(ArrayList<Entry> yData, ArrayList<String> xData) {
        getViewDataBinding().pieChart.setDrawHoleEnabled(true);
        getViewDataBinding().pieChart.setUsePercentValues(true);
        getViewDataBinding().pieChart.setHoleRadius(0);
        getViewDataBinding().pieChart.setTransparentCircleRadius(0);
        getViewDataBinding().pieChart.setRotationAngle(0);
        PieDataSet pieDataSet = new PieDataSet(yData, "Date");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData pieData = new PieData(xData, pieDataSet);
        pieData.setValueTextSize(9f);
        pieData.setValueTextColor(Color.BLACK);
        getViewDataBinding().pieChart.setData(pieData);
        getViewDataBinding().pieChart.invalidate();

    }

}
