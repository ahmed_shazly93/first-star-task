package com.shazly.firststartask.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shazly.firststartask.R;
import com.shazly.firststartask.databinding.FragmentProfileBinding;
import com.shazly.firststartask.utilities.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment<FragmentProfileBinding> {


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {

getViewDataBinding().email.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent e = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:" + "ahmedelsayed_1993@hotmail.com");
        e.setData(data);
        startActivity(e);
    }
});getViewDataBinding().phoneNumber.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "00201155348462"));
        startActivity(intent);
    }
});getViewDataBinding().portfolio.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ahmed-shazly.com/en"));
         startActivity(intent);
    }
});

    }
}
