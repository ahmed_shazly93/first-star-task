package com.shazly.firststartask.api;


import com.shazly.firststartask.model.BusinessModel;
import com.shazly.firststartask.model.ChartModel;
import com.shazly.firststartask.model.CityModel;
import com.shazly.firststartask.model.CountryModel;
import com.shazly.firststartask.model.RegisterModel;
import com.shazly.firststartask.model.ShopTypeModel;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetroService {


    @GET("DriverService.asmx/GetCountry")
    Single<CountryModel> getCountry();

    @FormUrlEncoded
    @POST("DriverService.asmx/GetCity")
    Single<CityModel> getCity(@Field("CountryID") String CountryID);

    @GET("MerchantService.asmx/GetBusinessType")
    Single<BusinessModel> getBusinessType();

    @FormUrlEncoded
    @POST("MerchantService.asmx/GetShopType")
    Single<ShopTypeModel> getShopType(@Field("BusinessTypeID") String BusinessTypeID);

    @FormUrlEncoded
    @POST("MerchantService.asmx/Merchant_SignUp_Test")
    Single<RegisterModel> signUp(
            @Field("MerchantName") String MerchantName,
            @Field("OwenrName") String OwenrName,
            @Field("PhoneNumber") String PhoneNumber,
            @Field("Country") String Country,
            @Field("City") String City,
            @Field("BusinessType") String BusinessType,
            @Field("ShopType") String ShopType,
            @Field("StreetName") String StreetName,
            @Field("BuildingNumber") String BuildingNumber,
            @Field("FloorNumber") String FloorNumber,
            @Field("NearestLocation") String NearestLocation);


    @FormUrlEncoded
    @POST("MerchantService.asmx/Merchant_ChartHome_Test")
    Single<ChartModel> getChartData(
            @Field("VerificationCode") String VerificationCode,
            @Field("MerchantsID") int MerchantsID );

}
