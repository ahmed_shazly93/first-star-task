package com.shazly.firststartask.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shazly.firststartask.MainActivity;
import com.shazly.firststartask.R;
import com.shazly.firststartask.databinding.ActivityNotificationBinding;
import com.shazly.firststartask.utilities.BaseActivity;

import java.util.concurrent.TimeUnit;

public class NotificationActivity extends BaseActivity<ActivityNotificationBinding> {
    boolean hasBack = false;
    CountDownTimer countDownTimer;
    @Override
    public int getLayoutId() {
        return R.layout.activity_notification;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    public void onBackPressed() {
        if (!hasBack) {
            startActivity(new Intent(NotificationActivity.this, MainActivity.class));
        } else {
            super.onBackPressed();
        }
    }

    private void init() {
        hasBack = getIntent().getBooleanExtra("hasBack", false);
        startCountDownTimer();

    }


    private void startCountDownTimer() {
        try {
            countDownTimer = new CountDownTimer(10000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    getViewDataBinding().textTimer.setText(hmsTimeFormatter(millisUntilFinished));

                    getViewDataBinding().progressBar.setProgress((int) (millisUntilFinished / 1000));

                    getViewDataBinding().progressBar.setMax(10);
                }

                @Override
                public void onFinish() {
                    getViewDataBinding().textTimer.setText("00:00:00");
                    onBackPressed();

                }

            }.start();
            countDownTimer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String hmsTimeFormatter(long milliSeconds) {

        @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliSeconds) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliSeconds)),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
        return hms;
    }
}
