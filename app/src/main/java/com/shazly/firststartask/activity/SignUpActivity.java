package com.shazly.firststartask.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shazly.firststartask.R;
import com.shazly.firststartask.adapter.BusinessAdapter;
import com.shazly.firststartask.adapter.CityAdapter;
import com.shazly.firststartask.adapter.CountryAdapter;
import com.shazly.firststartask.adapter.ShopTypeAdapter;
import com.shazly.firststartask.api.RetroService;
import com.shazly.firststartask.api.RetrofitClient;
import com.shazly.firststartask.databinding.ActivitySignUpBinding;
import com.shazly.firststartask.model.BusinessModel;
import com.shazly.firststartask.model.CityModel;
import com.shazly.firststartask.model.CountryModel;
import com.shazly.firststartask.model.RegisterModel;
import com.shazly.firststartask.model.ShopTypeModel;
import com.shazly.firststartask.utilities.BaseActivity;
import com.shazly.firststartask.utilities.ResourceUtil;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SignUpActivity extends BaseActivity<ActivitySignUpBinding> {
    RetroService retroService;
    CompositeDisposable compositeDisposable;
    Toolbar toolbar;
    TextView toolbar_title;
    CountryAdapter countryAdapter;
    CityAdapter cityAdapter;
    BusinessAdapter businessAdapter;
    ShopTypeAdapter shopAdapter;
    ArrayList<CountryModel.InfoBean> countryArrayList = new ArrayList<>();
    public ArrayList<CityModel.InfoBean> cityArrayList = new ArrayList<>();
    ArrayList<BusinessModel.InfoBean> businessArrayList = new ArrayList<>();
    public ArrayList<ShopTypeModel.InfoBean> shopTypeArrayList = new ArrayList<>();
    public String country_id = "", city_id = "", business_type = "", shop_type = "";
    public BottomSheetBehavior sheetBehavior;
    LinearLayout layoutBottomSheet;
    RecyclerView recyclerView;
    TextView choose_title;

    //    AddressAdapter adapter;
    @Override
    public int getLayoutId() {
        return R.layout.activity_sign_up;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        retroService = RetrofitClient.getClient().create(RetroService.class);
        compositeDisposable = new CompositeDisposable();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("");
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText(getResources().getString(R.string.sign_up));
        if (ResourceUtil.getCurrentLanguage(this).equals("ar")) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_ar);
        } else {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_en);
        }
// Bottom Sheet
        layoutBottomSheet = findViewById(R.id.bottom_sheet);
        recyclerView = findViewById(R.id.bottom_recycler);
        choose_title = findViewById(R.id.choose_title);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });


        getViewDataBinding().country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceUtil.hideKeyboard(SignUpActivity.this);

                if (countryArrayList.size() > 0) {
                    choose_title.setText(getResources().getString(R.string.eg_country));
                    countryAdapter = new CountryAdapter(countryArrayList, SignUpActivity.this);
                    recyclerView.setAdapter(countryAdapter);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });
        getViewDataBinding().city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceUtil.hideKeyboard(SignUpActivity.this);

                if (cityArrayList.size() > 0 && !country_id.isEmpty()) {
                    choose_title.setText(getResources().getString(R.string.eg_city));
                    cityAdapter = new CityAdapter(cityArrayList, SignUpActivity.this);
                    recyclerView.setAdapter(cityAdapter);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    Toast.makeText(SignUpActivity.this, getString(R.string.eg_country), Toast.LENGTH_SHORT).show();
                }
            }
        });
        getViewDataBinding().businessType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceUtil.hideKeyboard(SignUpActivity.this);
                if (businessArrayList.size() > 0) {
                    choose_title.setText(getResources().getString(R.string.eg_business_type));
                    businessAdapter = new BusinessAdapter(businessArrayList, SignUpActivity.this);
                    recyclerView.setAdapter(businessAdapter);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });
        getViewDataBinding().shopType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceUtil.hideKeyboard(SignUpActivity.this);

                if (shopTypeArrayList.size() > 0 && !business_type.equals("")) {
                    choose_title.setText(getResources().getString(R.string.eg_shop_type));
                    shopAdapter = new ShopTypeAdapter(shopTypeArrayList, SignUpActivity.this);
                    recyclerView.setAdapter(shopAdapter);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    Toast.makeText(SignUpActivity.this, getString(R.string.eg_business_type), Toast.LENGTH_SHORT).show();
                }
            }
        });
        getViewDataBinding().signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkText()) {
                    signUp();
                }
            }
        });
        getCountry();
        getBusinessType();

    }


    public void getCountry() {
        if (ResourceUtil.isNetworkAvailable(this)) {
            showProgressDialog();
            compositeDisposable.add(
                    retroService.getCountry().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<CountryModel>() {
                                @Override
                                public void onSuccess(CountryModel model) {
                                    dismissProgressDialg();

                                    if (model.getInfo().size() > 0) {
                                        countryArrayList.clear();
                                        countryArrayList.addAll(model.getInfo());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialg();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(this).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }
    }

    public void getCity(String id) {
        if (ResourceUtil.isNetworkAvailable(this)) {
            showProgressDialog();
            compositeDisposable.add(
                    retroService.getCity(id).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<CityModel>() {
                                @Override
                                public void onSuccess(CityModel model) {
                                    dismissProgressDialg();

                                    if (model.getInfo().size() > 0) {
                                        cityArrayList.clear();
                                        cityArrayList.addAll(model.getInfo());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialg();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(this).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }
    }

    public void getBusinessType() {
        if (ResourceUtil.isNetworkAvailable(this)) {
            showProgressDialog();
            compositeDisposable.add(
                    retroService.getBusinessType().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<BusinessModel>() {
                                @Override
                                public void onSuccess(BusinessModel model) {
                                    dismissProgressDialg();

                                    if (model.getInfo().size() > 0) {

                                        businessArrayList.clear();
                                        businessArrayList.addAll(model.getInfo());
//
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialg();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(this).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }
    }

    public void getShopType(String id) {
        if (ResourceUtil.isNetworkAvailable(this)) {
            showProgressDialog();
            compositeDisposable.add(
                    retroService.getShopType(id).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<ShopTypeModel>() {
                                @Override
                                public void onSuccess(ShopTypeModel model) {
                                    dismissProgressDialg();

                                    if (model.getInfo().size() > 0) {
                                        shopTypeArrayList.clear();
                                        shopTypeArrayList.addAll(model.getInfo());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialg();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(this).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }
    }

    public void signUp() {
        if (ResourceUtil.isNetworkAvailable(this)) {
            showProgressDialog();
            compositeDisposable.add(
                    retroService.signUp(
                            getViewDataBinding().merchantName.getText().toString(),
                            getViewDataBinding().ownerName.getText().toString(),
                            getViewDataBinding().phoneNumber.getText().toString(),
                            country_id,
                            city_id,
                            business_type,
                            shop_type,
                            getViewDataBinding().streetName.getText().toString(),
                            getViewDataBinding().buildingNumber.getText().toString(),
                            getViewDataBinding().floorNumber.getText().toString(),
                            getViewDataBinding().nearestLocation.getText().toString()
                    ).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<RegisterModel>() {
                                @Override
                                public void onSuccess(RegisterModel model) {
                                    dismissProgressDialg();

                                    if (model != null) {
                                        showAlert(model.getInfo().get(0).getResult());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialg();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(this).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }
    }

    private boolean checkText() {


        if (getViewDataBinding().merchantName.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.eg_merchant_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (getViewDataBinding().ownerName.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.owner_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (getViewDataBinding().phoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.phone_number), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (country_id.isEmpty()) {
            Toast.makeText(this, getString(R.string.eg_country), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (city_id.isEmpty()) {
            Toast.makeText(this, getString(R.string.eg_city), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (business_type.isEmpty()) {
            Toast.makeText(this, getString(R.string.eg_business_type), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (shop_type.isEmpty()) {
            Toast.makeText(this, getString(R.string.eg_shop_type), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (getViewDataBinding().streetName.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.street_name), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (getViewDataBinding().floorNumber.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.floor_number), Toast.LENGTH_SHORT).show();
            return false;

        }
        if (getViewDataBinding().nearestLocation.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.eg_nearest_location), Toast.LENGTH_SHORT).show();
            return false;

        }

        return true;

    }

    public void showAlert(final String message) {
        try {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(SignUpActivity.this);
            dlgAlert.setMessage(message);
            dlgAlert.setTitle("");
            dlgAlert.setPositiveButton(getString(R.string.done), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onBackPressed();
                }
            });
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        } catch (Exception e) {
            Log.v("ERROR", e.getMessage());
        }
    }

}
