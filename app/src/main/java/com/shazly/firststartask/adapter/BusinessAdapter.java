package com.shazly.firststartask.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shazly.firststartask.R;
import com.shazly.firststartask.activity.SignUpActivity;
import com.shazly.firststartask.databinding.CustomSpinnerItemBinding;
import com.shazly.firststartask.model.BusinessModel;
import com.shazly.firststartask.model.CityModel;

import java.util.ArrayList;

/**
 *
 */

public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.ViewHolder> {

    ArrayList<BusinessModel.InfoBean> businessArrayList;
    SignUpActivity signUpActivity;

    public BusinessAdapter(ArrayList<BusinessModel.InfoBean> businessArrayList, SignUpActivity signUpActivity) {
        this.businessArrayList = businessArrayList;
        this.signUpActivity = signUpActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomSpinnerItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.custom_spinner_item,
                parent, false);
        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        BusinessModel.InfoBean model = businessArrayList.get(position);
        holder.custmerBinding.text.setText(model.getBusinessType());

        holder.custmerBinding.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpActivity.business_type = model.getId();
                signUpActivity.shop_type = "";
                signUpActivity.getViewDataBinding().shopType.setText("");
                signUpActivity.getViewDataBinding().businessType.setText(model.getBusinessType());
                signUpActivity.getShopType(model.getId());
                signUpActivity.sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


            }
        });

    }

    @Override
    public int getItemCount() {
        return businessArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomSpinnerItemBinding custmerBinding;

        public ViewHolder(CustomSpinnerItemBinding layoutBinding) {
            super(layoutBinding.getRoot());
            custmerBinding = layoutBinding;
        }
    }
}
