package com.shazly.firststartask.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shazly.firststartask.R;
import com.shazly.firststartask.activity.SignUpActivity;
import com.shazly.firststartask.databinding.CustomSpinnerItemBinding;
import com.shazly.firststartask.model.BusinessModel;
import com.shazly.firststartask.model.ShopTypeModel;

import java.util.ArrayList;

/**
 *
 */

public class ShopTypeAdapter extends RecyclerView.Adapter<ShopTypeAdapter.ViewHolder> {

    ArrayList<ShopTypeModel.InfoBean> shopTypeArrayList;
    SignUpActivity signUpActivity;

    public ShopTypeAdapter(ArrayList<ShopTypeModel.InfoBean> shopTypeArrayList, SignUpActivity signUpActivity) {
        this.shopTypeArrayList = shopTypeArrayList;
        this.signUpActivity = signUpActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomSpinnerItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.custom_spinner_item,
                parent, false);
        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        ShopTypeModel.InfoBean model = shopTypeArrayList.get(position);
        holder.custmerBinding.text.setText(model.getShopTyep());

        holder.custmerBinding.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signUpActivity.shop_type = model.getId();
                signUpActivity.getViewDataBinding().shopType.setText(model.getShopTyep());
                signUpActivity.sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });

    }

    @Override
    public int getItemCount() {
        return shopTypeArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomSpinnerItemBinding custmerBinding;

        public ViewHolder(CustomSpinnerItemBinding layoutBinding) {
            super(layoutBinding.getRoot());
            custmerBinding = layoutBinding;
        }
    }
}
