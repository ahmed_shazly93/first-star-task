package com.shazly.firststartask.adapter;

import android.databinding.DataBindingUtil;
import android.location.Address;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shazly.firststartask.R;
import com.shazly.firststartask.databinding.CustmerDeliveryAddressBinding;
import com.shazly.firststartask.fragment.CurrentLocationFragment;
import com.shazly.firststartask.fragment.MapFragment;

import java.util.List;

/**
 *
 */

public class LocationAddressAdapter extends RecyclerView.Adapter<LocationAddressAdapter.ViewHolder> {
    CurrentLocationFragment currentLocationFragment;
    List<Address> arrayList;
    MapFragment activity;
    int from;

    public LocationAddressAdapter(List<Address> arrayList, MapFragment activity, int from) {
        this.arrayList = arrayList;
        this.activity = activity;
        this.from = from;
    }

    public LocationAddressAdapter( List<Address> arrayList,CurrentLocationFragment currentLocationFragment, int from) {
        this.currentLocationFragment = currentLocationFragment;
        this.arrayList = arrayList;
        this.from = from;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustmerDeliveryAddressBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.custmer_delivery_address,
                parent, false);
        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Address model = arrayList.get(position);
        holder.custmerBinding.name.setText(model.getFeatureName() + " - " + model.getSubAdminArea() + " - " + model.getAdminArea());

        holder.custmerBinding.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from == 1) {
                    activity.goTo(model);
                    activity.addressList.clear();
                    notifyDataSetChanged();
                } else {
                    currentLocationFragment.goTo(model);
                    currentLocationFragment.addressList.clear();
                    notifyDataSetChanged();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustmerDeliveryAddressBinding custmerBinding;

        public ViewHolder(CustmerDeliveryAddressBinding layoutBinding) {
            super(layoutBinding.getRoot());
            custmerBinding = layoutBinding;
        }
    }
}
