package com.shazly.firststartask.adapter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shazly.firststartask.R;
import com.shazly.firststartask.activity.SignUpActivity;
import com.shazly.firststartask.databinding.CustomSpinnerItemBinding;
import com.shazly.firststartask.model.CountryModel;
import com.shazly.firststartask.utilities.ResourceUtil;

import java.util.ArrayList;

/**
 *
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    ArrayList<CountryModel.InfoBean> countryArrayList;
    SignUpActivity signUpActivity;

    public CountryAdapter(ArrayList<CountryModel.InfoBean> countryArrayList, SignUpActivity signUpActivity) {
        this.countryArrayList = countryArrayList;
        this.signUpActivity = signUpActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomSpinnerItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.custom_spinner_item,
                parent, false);
        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        CountryModel.InfoBean model = countryArrayList.get(position);
        holder.custmerBinding.text.setText(model.getCountryName());

        holder.custmerBinding.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpActivity.country_id = model.getId();
                signUpActivity.city_id = "";
                signUpActivity.getViewDataBinding().city.setText("");
                signUpActivity.getViewDataBinding().country.setText(model.getCountryName());
                 signUpActivity.getCity(model.getId());
                signUpActivity.sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

    }

    @Override
    public int getItemCount() {
        return countryArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomSpinnerItemBinding custmerBinding;

        public ViewHolder(CustomSpinnerItemBinding layoutBinding) {
            super(layoutBinding.getRoot());
            custmerBinding = layoutBinding;
        }
    }
}
