package com.shazly.firststartask.model;

import java.util.List;

public class ChartModel {

    /**
     * Result : 1
     * info : [{"id":1,"NumberofOrders":1,"date":"01/12","OverAllResult":1},{"id":1,"NumberofOrders":1,"date":"02/12","OverAllResult":1},{"id":1,"NumberofOrders":4,"date":"03/12","OverAllResult":1},{"id":1,"NumberofOrders":1,"date":"04/11","OverAllResult":1},{"id":1,"NumberofOrders":7,"date":"06/01","OverAllResult":1},{"id":1,"NumberofOrders":1,"date":"06/02","OverAllResult":1},{"id":1,"NumberofOrders":4,"date":"06/10","OverAllResult":1}]
     */

    private int Result;
    private List<InfoBean> info;

    public int getResult() {
        return Result;
    }

    public void setResult(int Result) {
        this.Result = Result;
    }

    public List<InfoBean> getInfo() {
        return info;
    }

    public void setInfo(List<InfoBean> info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * id : 1
         * NumberofOrders : 1
         * date : 01/12
         * OverAllResult : 1
         */

        private int id;
        private int NumberofOrders;
        private String date;
        private int OverAllResult;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getNumberofOrders() {
            return NumberofOrders;
        }

        public void setNumberofOrders(int NumberofOrders) {
            this.NumberofOrders = NumberofOrders;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getOverAllResult() {
            return OverAllResult;
        }

        public void setOverAllResult(int OverAllResult) {
            this.OverAllResult = OverAllResult;
        }
    }
}
