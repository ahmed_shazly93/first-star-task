package com.shazly.firststartask.model;

import java.util.List;

public class ShopTypeModel {

    /**
     * Result : 1
     * info : [{"Id":1,"ShopTyep":"Shop Type 1","OverAllResult":1},{"Id":2,"ShopTyep":"Shop Type 2","OverAllResult":1},{"Id":3,"ShopTyep":"Shop Type 3","OverAllResult":1}]
     */

    private int Result;
    private List<InfoBean> info;

    public int getResult() {
        return Result;
    }

    public void setResult(int Result) {
        this.Result = Result;
    }

    public List<InfoBean> getInfo() {
        return info;
    }

    public void setInfo(List<InfoBean> info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * Id : 1
         * ShopTyep : Shop Type 1
         * OverAllResult : 1
         */

        private String Id;
        private String ShopTyep;
        private int OverAllResult;

        public InfoBean(String id, String shopTyep, int overAllResult) {
            Id = id;
            ShopTyep = shopTyep;
            OverAllResult = overAllResult;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getShopTyep() {
            return ShopTyep;
        }

        public void setShopTyep(String ShopTyep) {
            this.ShopTyep = ShopTyep;
        }

        public int getOverAllResult() {
            return OverAllResult;
        }

        public void setOverAllResult(int OverAllResult) {
            this.OverAllResult = OverAllResult;
        }
    }
}
