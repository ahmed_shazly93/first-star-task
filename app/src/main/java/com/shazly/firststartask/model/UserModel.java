package com.shazly.firststartask.model;

public class UserModel {

    /**
     * status : true
     * message : تم تسجيل الدخول بنجاح
     * data : {"id":2,"name":"Abdelrahman ALgazzar","email":"algazzar.abdelrahman@gmail.com","phone":"01000478466","image":"https://points-app.com/storage/uploads/users/EnO4QjHhA8_1576970879.jpeg","points":0,"credit":0,"token":"K7HXFVTwwYl8qQDQwz4qomPhdhBBW1q6ZQQoKiNjVBTkcUnlIG06cr1d4CLnVkkT7UABvY"}
     */

    private boolean status;
    private String message;
    private DataBean data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * name : Abdelrahman ALgazzar
         * email : algazzar.abdelrahman@gmail.com
         * phone : 01000478466
         * image : https://points-app.com/storage/uploads/users/EnO4QjHhA8_1576970879.jpeg
         * points : 0
         * credit : 0
         * token : K7HXFVTwwYl8qQDQwz4qomPhdhBBW1q6ZQQoKiNjVBTkcUnlIG06cr1d4CLnVkkT7UABvY
         */

        private int id;
        private String name;
        private String email;
        private String phone;
        private String image;
        private float points;
        private float credit;
        private String token;

        public DataBean(int id, String name, String email, String phone, String image, float points, float credit, String token) {
            this.id = id;
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.image = image;
            this.points = points;
            this.credit = credit;
            this.token = token;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public float getPoints() {
            return points;
        }

        public void setPoints(float points) {
            this.points = points;
        }

        public float getCredit() {
            return credit;
        }

        public void setCredit(float credit) {
            this.credit = credit;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
