package com.shazly.firststartask.model;

import java.util.List;

public class BusinessModel {

    /**
     * Result : 1
     * info : [{"Id":1,"BusinessType":"BS 1","OverAllResult":1},{"Id":2,"BusinessType":"BS 2","OverAllResult":1}]
     */

    private int Result;
    private List<InfoBean> info;

    public int getResult() {
        return Result;
    }

    public void setResult(int Result) {
        this.Result = Result;
    }

    public List<InfoBean> getInfo() {
        return info;
    }

    public void setInfo(List<InfoBean> info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * Id : 1
         * BusinessType : BS 1
         * OverAllResult : 1
         */

        private String Id;
        private String BusinessType;
        private int OverAllResult;


        public InfoBean(String id, String businessType, int overAllResult) {
            Id = id;
            BusinessType = businessType;
            OverAllResult = overAllResult;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getBusinessType() {
            return BusinessType;
        }

        public void setBusinessType(String BusinessType) {
            this.BusinessType = BusinessType;
        }

        public int getOverAllResult() {
            return OverAllResult;
        }

        public void setOverAllResult(int OverAllResult) {
            this.OverAllResult = OverAllResult;
        }
    }
}
