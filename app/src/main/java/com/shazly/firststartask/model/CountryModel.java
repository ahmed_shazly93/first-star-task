package com.shazly.firststartask.model;

import java.util.List;

public class CountryModel {

    /**
     * Result : 1
     * info : [{"Id":100,"CountryName":"Jordan","OverAllResult":1}]
     */

    private int Result;
    private List<InfoBean> info;

    public int getResult() {
        return Result;
    }

    public void setResult(int Result) {
        this.Result = Result;
    }

    public List<InfoBean> getInfo() {
        return info;
    }

    public void setInfo(List<InfoBean> info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * Id : 100
         * CountryName : Jordan
         * OverAllResult : 1
         */

        private String Id;
        private String CountryName;
        private int OverAllResult;

        public InfoBean(String id, String countryName, int overAllResult) {
            Id = id;
            CountryName = countryName;
            OverAllResult = overAllResult;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getCountryName() {
            return CountryName;
        }

        public void setCountryName(String CountryName) {
            this.CountryName = CountryName;
        }

        public int getOverAllResult() {
            return OverAllResult;
        }

        public void setOverAllResult(int OverAllResult) {
            this.OverAllResult = OverAllResult;
        }
    }
}
