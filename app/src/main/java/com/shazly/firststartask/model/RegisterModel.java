package com.shazly.firststartask.model;

import java.util.List;

public class RegisterModel {


    /**
     * Result : 1
     * info : [{"Result":"Successfully added.","OverAllResult":1}]
     */

    private int Result;
    private List<InfoBean> info;

    public int getResult() {
        return Result;
    }

    public void setResult(int Result) {
        this.Result = Result;
    }

    public List<InfoBean> getInfo() {
        return info;
    }

    public void setInfo(List<InfoBean> info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * Result : Successfully added.
         * OverAllResult : 1
         */

        private String Result;
        private int OverAllResult;

        public String getResult() {
            return Result;
        }

        public void setResult(String Result) {
            this.Result = Result;
        }

        public int getOverAllResult() {
            return OverAllResult;
        }

        public void setOverAllResult(int OverAllResult) {
            this.OverAllResult = OverAllResult;
        }
    }
}
