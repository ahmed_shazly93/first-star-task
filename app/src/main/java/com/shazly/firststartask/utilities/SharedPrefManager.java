
package com.shazly.firststartask.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.shazly.firststartask.model.UserModel;


/**
 * Created by shazly on 22/2/2019.
 */


public class SharedPrefManager {

    //the constants
    public static final String SHARED_PREF_NAME = "simplifiedcodingsharedpref";
    private static final String SHARED_PREF_NAME1 = "simplifiedcodingsharedpref1";
    private static final String SHARED_PREF_NAME2 = "simplifiedcodingsharedpref2";
    private static final String SHARED_PREF_NAME3 = "simplifiedcodingsharedpref3";
    private static final String KEY_ID = "keyid";
    private static final String KEY_NAME = "keyName";
    private static final String KEY_MOBILE = "keymobile";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_Token = "keytoken";
    private static final String KEY_Points = "Points";
    private static final String KEY_Credit = "Credit";
    private static final String KEY_Lat = "keyLat";
    private static final String KEY_Lon = "keyLon";
    private static final String KEY_Licence = "KEY Licence";
    private static final String KEY_Cart_counter = "KEY Cart counter";
    private static final String KEY_Image = "Image";
    private static final String Key_Address = "Address";
    private static final String Key_average_rate = "Rate";
    private static final String Key_minimum_order = "Minimum order";
    private static final String Key_city = "City";
    private static final String Key_region_id = "Region Id";
    private static final String Key_region_name = "Region name";
    private static final String Key_exper_date = "exper date";
    private static final String Key_isChecked = "isCheched";
    private static final String Key_gram = "Gram";
    public static final String List = "List";
    public static final String OperatorList = "OperatorList";


    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }


    public void userLogin(UserModel.DataBean user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putString(KEY_NAME, user.getName());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_MOBILE, user.getPhone());
        editor.putString(KEY_Image, user.getImage());
        editor.putFloat(KEY_Points, user.getPoints());
        editor.putFloat(KEY_Credit, user.getCredit());
        editor.putString(KEY_Token, user.getToken());
        editor.apply();
//        editor.putInt(Key_average_rate, user.getAverage_rate());
//        editor.putInt(Key_minimum_order, user.getMinimum_order());
//        editor.putString(Key_Address, user.getAddress());
//        editor.putFloat(KEY_Lon, user.getLongitude());
//        editor.putFloat(KEY_Lat, user.getLatitude());
//        editor.putString(Key_city, user.getCity());
//        editor.putString(KEY_Bank_Name, user.getBank());
//        editor.putString(KEY_Bank_Account, user.getAccount());
//        editor.putInt(Key_region_id, user.getRegion().getId());
//        editor.putString(Key_region_name, user.getRegion().getName());
//        editor.putString(Key_exper_date, user.getExpire_date());
//        Gson category = new Gson();
//        String json = category.toJson(user.getLicence_images());
//        editor.putString(KEY_Category, json);
//        Gson licence = new Gson();
//        String json2 = licence.toJson(user.getLicence_images());
//        editor.putString(KEY_Licence, json);

    }

    public void setUserToken(String token) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_Token, token);
        editor.apply();
    }

    public void setCartCount(int cartCount) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME3, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(String.valueOf(KEY_Cart_counter), cartCount);
        editor.apply();
    }

    public void cityId(int cityid) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Key_city, cityid);
        editor.apply();
    }

    public void setIschecked(boolean isOpen) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME2, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Key_isChecked, isOpen);
        editor.apply();


    }

    //    public void setOperatorList(Set<String> operatorList) {
//        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME2, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putStringSet(OperatorList, operatorList);
//        editor.apply();
//
//    }
//    public String getOperatorList() {
//        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME2, Context.MODE_PRIVATE);
//        return sharedPreferences.getStringSet(OperatorList, null);
//    }
    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_Token, null) != null;
    }

    public boolean isChecked() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME2, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(Key_isChecked, false) != false;
    }

    //    this method will give the logged in user
    public UserModel.DataBean getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new UserModel.DataBean(
                sharedPreferences.getInt(KEY_ID, 0),
                sharedPreferences.getString(KEY_NAME, null),
                sharedPreferences.getString(KEY_EMAIL, null),
                sharedPreferences.getString(KEY_MOBILE, null),
                sharedPreferences.getString(KEY_Image, null),
                sharedPreferences.getFloat(KEY_Points, 0),
                sharedPreferences.getFloat(KEY_Credit, 0),
                sharedPreferences.getString(KEY_Token, null));

    }

    public String getToken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_Token, null);
    }

    public int getCartCount() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME3, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_Cart_counter, 0);
    }

    public int getCityId() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(Key_city, 0);
    }

    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        SharedPreferences sharedPreferences2 = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor2 = sharedPreferences2.edit();
        editor2.clear();
        editor2.apply();
    }


}


