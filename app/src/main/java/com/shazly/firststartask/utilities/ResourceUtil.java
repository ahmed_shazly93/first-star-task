package com.shazly.firststartask.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.shazly.firststartask.R;
import com.shazly.firststartask.model.CountryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.content.ContentValues.TAG;


public class ResourceUtil {
    static String mOld = "en";
    private static Locale myLocale;

    private static String[] STORAGE_PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static String[] CALL_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static String[] CALL_PERMISSIONS1 = {
            Manifest.permission.CALL_PHONE
    };

//    public static void changeMenuFont(NavigationView navigationView, Context context) {
//        Menu m = navigationView.getMenu();
//        for (int i = 0; i < m.size(); i++) {
//            MenuItem mi = m.getItem(i);
//
//            //for aapplying a font to subMenu ...
//            SubMenu subMenu = mi.getSubMenu();
//            if (subMenu != null && subMenu.size() > 0) {
//                for (int j = 0; j < subMenu.size(); j++) {
//                    MenuItem subMenuItem = subMenu.getItem(j);
//                    applyFontToMenuItem(subMenuItem, context);
//                }
//            }
//
//            //the method we have create in activity
//            applyFontToMenuItem(mi, context);
//
//        }
//
//    }
//
//    private static void applyFontToMenuItem(MenuItem mi, Context context) {
//        Typeface font = Typeface.createFromAsset(context.getAssets(), FONT_Normal_AR);
//        SpannableString mNewTitle = new SpannableString(mi.getTitle());
//        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        mi.setTitle(mNewTitle);
//    }



    public static void changeMenuFontBotton(BottomNavigationView bottomNavigationView, Context context) {
        Menu m = bottomNavigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem, context);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi, context);

        }

    }

    private static void applyFontToMenuItem(MenuItem mi, Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "sst-arabic-bold.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }



    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            Log.d(TAG, "isNetworkAvailable: True");
            return true;
        }
        Log.d(TAG, "isNetworkAvailable: False");
        return false;
    }

    public static void shareText(Context context, String text) {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");// Plain format text

        sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(sharingIntent, "Share Text Using " +
                context.getString(R.string.app_name)));
    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public static void changeLang(String lang, Context context) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        saveLocale(lang, context);
        if (!mOld.equals(lang)) {
//            Intent i = getPackageManager()
//                    .getLaunchIntentForPackage(getPackageName());
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(i);
        }
    }


    public static void saveLocale(String lang, Context context) {
        String langPref = context.getPackageName() + "App_Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }


    public static String getCurrentLanguage(Context context) {
        String langPref = context.getPackageName() + "App_Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String old = prefs.getString(langPref, "ar");
        return old;
    }



    public static void setTapFont(TabLayout tabLayout, Context context) {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {

                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(context.getAssets(), "font.ttf"));
                    ((TextView) tabViewChild).setTextSize(8.0f);
                    ((TextView) tabViewChild).setGravity(Gravity.BOTTOM);
                    ((TextView) tabViewChild).setPadding(0, 8, 0, 0);
                }
            }
        }
    }


    public static void setSpinnerCustomAdubter(Spinner spinner, ArrayList<String> list, int layId, Context context) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                context.getApplicationContext(), layId, list);
        adapter.setDropDownViewResource(layId);
        spinner.setAdapter(adapter);
    }
    public static void setSpinnerCustomAdabterCountry(Spinner spinner, ArrayList<CountryModel.InfoBean> list, int layId, Context context) {
        ArrayAdapter<CountryModel.InfoBean> adapter = new ArrayAdapter<CountryModel.InfoBean>(
                context.getApplicationContext(), layId, list);
        adapter.setDropDownViewResource(layId);
        spinner.setAdapter(adapter);

    }


    public static void callNumber(String number, Context context) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        context.startActivity(callIntent);
    }


    public static void verifyPermissions(Activity context) {
        int permissionCall = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionExternal = ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionCall != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context,
                    CALL_PERMISSIONS,
                    0);
        }


        if (permissionExternal != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context,
                    CALL_PERMISSIONS,
                    0);
        }

    }

    public static void verifyPermissions1(Activity context) {
        int permissionCall = ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        // int permissionExternal = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCall != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context,
                    CALL_PERMISSIONS1,
                    1);
        }

    }

//    public static void changeBottomNavagationFont(BottomNavigationView navigationView, Context context) {
//        Menu m = navigationView.getMenu();
//        for (int i = 0; i < m.size(); i++) {
//            MenuItem mi = m.getItem(i);
//
//            //for aapplying a font to subMenu ...
//            SubMenu subMenu = mi.getSubMenu();
//            if (subMenu != null && subMenu.size() > 0) {
//                for (int j = 0; j < subMenu.size(); j++) {
//                    MenuItem subMenuItem = subMenu.getItem(j);
//                    applyFontToMenuItem(subMenuItem, context);
//                }
//            }
//
//            //the method we have create in activity
//            applyFontToMenuItem(mi, context);
//
//        }
//
//    }


    public static boolean getNotification(Context c) {
        String notiPref = "App_Notification1";
        SharedPreferences prefs = c.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        boolean isLogin = prefs.getBoolean(notiPref, true);
        return isLogin;
    }

    public static void saveNotification(boolean val, Context c) {
        String notiPref = "App_Notification1";
        SharedPreferences prefs = c.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(notiPref, val);
        editor.commit();
    }


    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getDate(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }

    public static String getTime(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }

    public static String getCompleteDate(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }


    public static void saveToken(String token, Context context) {
        String langPref = context.getPackageName() + "Token";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, token);
        editor.commit();
    }


    public static String getToken(Context context) {
        String langPref = context.getPackageName() + "Token";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String old = prefs.getString(langPref, "");
        return old;
    }




    public static void saveIsLogin(boolean login, Context context) {
        String langPref = "login";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(langPref, login);
        editor.commit();
    }

    public static boolean isLogin(Context context) {
        String langPref = "login";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        boolean login = prefs.getBoolean(langPref, false);
        return login;
    }




    public static String parseDateToEEEMMMd(String dateStr, String inputFormatStr, String outputFormatStr) {

        SimpleDateFormat outputFormat = new SimpleDateFormat(outputFormatStr, Locale.ENGLISH);
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputFormatStr);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(dateStr);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}